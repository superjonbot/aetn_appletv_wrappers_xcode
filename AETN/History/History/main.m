//
//  main.m
//  Apple TV Service
//

#import <UIKit/UIKit.h>
#import <TVMLKit/TVMLKit.h>

int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *logConfig = [standardUserDefaults arrayForKey:@"TVSLogConfig"];
        logConfig = logConfig ? : [NSArray array];
        NSSet *componentSet = [NSSet setWithArray:logConfig];
        componentSet = [componentSet setByAddingObjectsFromArray:@[@"JS"]];
        NSArray *newLogConfig = [componentSet allObjects];
        
        [standardUserDefaults setObject:newLogConfig forKey:@"TVSLogConfig"];
        [standardUserDefaults setObject:@(YES) forKey:@"JavaScriptCoreOutputConsoleMessagesToSystemConsole"];
        [standardUserDefaults setObject:@(YES) forKey:@"TracingEnabled"];
        [standardUserDefaults setObject:@(YES) forKey:@"LogJSConsole"];
        
        return UIApplicationMain(argc, argv, @"TVLApplication", @"TVLAppDelegate");
    }
}